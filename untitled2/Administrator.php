<?php

namespace untitled2;

 class Administrator extends Human
{
    use BanUser;
    
    public function addUser($id, $name, $email):User
    {
        return new User($id, $name, $email);
    }
    
    public function removeUser($id):string
    {
        return "User with id = $id was removed";
    }
    
    public function addOrganizator($id, $name, $email):Organizator
    {
        return new Organizator($id, $name, $email);
    }
    
    public function removeOrganizator($id):string
    {
        return "Organizator with id = $id was removed";
    }
    
}
