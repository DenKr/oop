<?php

namespace untitled2;

abstract class Human
{
    protected string $name;
    protected string $email;
    protected int    $id;
    
    public function __construct($id, $name, $email)
    {
        $this->id    = $id;
        $this->name  = $name;
        $this->email = $email;
    }
    
    protected function getName()
    {
        return $this->name;
    }
    
    protected function getEmail()
    {
        return $this->email;
    }
    
    protected function getId()
    {
        return $this->id;
    }
    
    protected function getMailRecoveryPassword(string $to, string $subject, string $message)
    {
        mail($to, $subject, $message);
    }
}
